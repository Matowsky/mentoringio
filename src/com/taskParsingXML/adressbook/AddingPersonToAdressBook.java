package com.taskParsingXML.adressbook;

import java.util.ArrayList;

public class AddingPersonToAdressBook {
    public static void main(String[] args) {
        WriteFile ob = new WriteFile();
        ArrayList addressList = new ArrayList();
        addressList.add(new adressBook(1,"Matthew", "Kos", "Gdynia"));
        addressList.add(new adressBook(2,"John", "Kosk", "Gdynia"));
        addressList.add(new adressBook(3,"Stephen", "King", "Sopot"));

        ob.writeXmlFile(addressList);
    }

}
