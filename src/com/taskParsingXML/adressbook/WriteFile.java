package com.taskParsingXML.adressbook;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WriteFile {
    public void writeXmlFile(ArrayList<adressBook> list) {

        try {

            DocumentBuilderFactory dFact = DocumentBuilderFactory.newInstance();
            DocumentBuilder build = dFact.newDocumentBuilder();
            Document doc = build.newDocument();

            Element root = doc.createElement("AdressBook");
            doc.appendChild(root);

            Element Details = doc.createElement("Person");
            root.appendChild(Details);

            for (adressBook adress: list) {

                Element id = doc.createElement("id");
                id.appendChild(doc.createTextNode(String.valueOf(adress.getId())));
                Details.appendChild(id);

                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(String.valueOf(adress.getName())));
                Details.appendChild(name);


                Element surname = doc.createElement("surname");
                surname.appendChild(doc.createTextNode(String.valueOf(adress.getSurname())));
                Details.appendChild(surname);

                Element city = doc.createElement("city");
                city.appendChild(doc.createTextNode(String.valueOf(adress.getCity())));
                Details.appendChild(city);

            }


            // Save the document to the disk file
            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            // format the XML nicely
            aTransformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");

            aTransformer.setOutputProperty(
                    "{http://xml.apache.org/xslt}indent-amount", "4");
            aTransformer.setOutputProperty(OutputKeys.INDENT, "yes");


            DOMSource source = new DOMSource(doc);
            try {
                FileWriter fos = new FileWriter("/workspace/adressBook.xml");
                StreamResult result = new StreamResult(fos);
                aTransformer.transform(source, result);

            } catch (IOException e) {

                e.printStackTrace();
            }


        } catch (TransformerException ex) {
            System.out.println("Error outputting document");

        } catch (ParserConfigurationException ex) {
            System.out.println("Error building document");
        }

    }
}

