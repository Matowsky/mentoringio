package com.taskParsingXML.jaxb;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public class MainClass {
    public static void main(String[] args) throws JAXBException, IOException {
        Client client1 = new Client();
        JAXBWrite write = new JAXBWrite();
        JAXBReader reader = new JAXBReader();
        SortClient sort = new SortClient();
        client1.setId(1);
        client1.setName("Jan");
        client1.setSurname("Ul");
        client1.setCity("Gdańsk");

        Client client2 = new Client();
        client2.setId(2);
        client2.setName("Zdzich");
        client2.setSurname("Ul");
        client2.setCity("Sopot");

        Client client3 = new Client();
        client3.setId(3);
        client3.setName("John");
        client3.setSurname("Fik");
        client3.setCity("Sopot");

        Client client4 = new Client();
        client4.setId(4);
        client4.setName("Irma");
        client4.setSurname("Fuk");
        client4.setCity("Sopot");

        DataClient dataClient = new DataClient();
        dataClient.getListOfClients().add(client1);
        dataClient.getListOfClients().add(client2);
        dataClient.getListOfClients().add(client3);
        dataClient.getListOfClients().add(client4);


        System.out.println("Dokonano zapisu");
        write.jABXWrite(dataClient);
        System.out.println("Odczyt:");
        reader.jAXBReader();
        System.out.println("Odczyt według kryterium:");
        sort.sortClient(dataClient);
    }
}
