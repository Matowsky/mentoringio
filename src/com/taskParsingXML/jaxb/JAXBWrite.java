package com.taskParsingXML.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import java.io.FileWriter;
import java.io.IOException;

public class JAXBWrite {
    public  void jABXWrite(DataClient dataClient) {
        try {
            JAXBContext context = JAXBContext.newInstance(DataClient.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(dataClient, new FileWriter("/workspace/listOfClient.xml"));
        }catch (JAXBException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
