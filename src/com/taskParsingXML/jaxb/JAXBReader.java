package com.taskParsingXML.jaxb;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import java.io.File;

public class JAXBReader {
    public void jAXBReader() {
        try {
            File file = new File("/workspace/listOfClient.xml");
            JAXBContext jaxbContext = JAXBContext.newInstance(DataClient.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            DataClient dataClient = (DataClient) jaxbUnmarshaller.unmarshal(file);
            for(int i =0; i<dataClient.getListOfClients().size();i++) {
                System.out.println(dataClient.getListOfClients().get(i));
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}

