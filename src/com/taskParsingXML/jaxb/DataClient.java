package com.taskParsingXML.jaxb;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name="person")
@XmlAccessorType(XmlAccessType.FIELD)
public class DataClient {

    @XmlElement(name="listOfPerson")
    private  List<Client> listOfClients = new ArrayList<>();

    public List<Client> getListOfClients() {
        return listOfClients;
    }

    public void setListOfClients(List<Client> listOfClients) {
        this.listOfClients = listOfClients;
    }
}
