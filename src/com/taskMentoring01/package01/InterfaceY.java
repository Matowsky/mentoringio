package com.taskMentoring01.package01;

/**
 * @author m016392 on 2017-11-15.
 */
public interface InterfaceY {
	void methodY01();
	void methodY02();
}
