package com.taskMentoring01.package01;

/**
 * @author m016392 on 2017-11-15.
 */
public class ExampleClass02 implements InterfaceX,InterfaceY,InterfaceZ {
    @Override
    public void methodX01() {

    }

    @Override
    public void methodX02() {

    }

    @Override
    public void methodY01() {

    }

    @Override
    public void methodY02() {

    }

    @Override
    public void methodZ01() {

    }

    @Override
    public void methodZ02() {

    }
}
