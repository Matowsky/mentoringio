package com.taskMentoring01.package01;

/**
 * @author m016392 on 2017-11-15.
 */
public interface InterfaceX {
    void methodX01();
    void methodX02();
}
