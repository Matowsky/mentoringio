package com.taskMentoring01.package02;

import java.util.Scanner;
/**
 * @author m016392 on 2017-11-15.
 */
public class ValidatorInputString {
    private static int sum;
    private static boolean nameOrSurname=false;
    private static boolean dateOfBirth=false;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Decide want you want to do:");
        System.out.println("1 - validate name");
        System.out.println("2 - validate surname");
        System.out.println("3 - validate date of birth format dd-mm-yyyy");
        System.out.println("4 - quit");
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            if (line.equals("1")) {
                System.out.println("Give name!");
                while(nameOrSurname==false) {
                    String lineName = sc.next();
                    validateNameAndSurname(lineName);
                }
                continue;
            } else if (line.equals("2")) {
                System.out.println("Give surname!");
                while(nameOrSurname==false) {
                    String lineSurname = sc.next();
                    validateNameAndSurname(lineSurname);
                }
                continue;
            } else if (line.equals("3")) {
                System.out.println("Give date of birth! Format dd-mm-yyyy");
                while(dateOfBirth==false) {
                    String lineDateOfBirth = sc.next();
                    validateDateOfBirth(lineDateOfBirth);
                }
                continue;
            } else if (line.equals("4")) {
                System.out.println("Quit!");
                break;
            }

        }
    }
        	private static void validateDateOfBirth(String line) {
                validateDelimeter(line);
                if(sum==2) {
                    String[] partOfDateOfBirth = line.split("-");
                    String parts1 = partOfDateOfBirth[0];
                    String parts2 = partOfDateOfBirth[1];
                    String parts3 = partOfDateOfBirth[2];
                    if (parts1.matches("[0-9]+") && parts2.matches("[0-9]+") && parts3.matches("[0-9]+") && parts1.length() == 2
                            && parts2.length() == 2 && parts3.length() == 4) {
                        System.out.println("Correct, give a next number");
                        dateOfBirth=true;
                    } else {
                        System.out.println("Incorrect.Try again! Format: dd-mm-yyyy");
                    }
                }
                else{
                    System.out.println("Incorrect.Try again!");
                    sum=0;
                }

        	}
    public static int validateDelimeter(String line) {
        sum =0;
        for(int i=0; i<line.length(); i++){
            String x = line.substring(i,i+1);
            if(x.contains("-")){
               sum++;
            }
        }
        return sum;
    }

    private static void validateNameAndSurname(String line) {
                if (!line.matches("[a-zA-Z]+") || line.equals("")) {
                    System.out.println("Incorrect.Try again!");
                } else {
                    System.out.println("Correct, give a next number");
                    nameOrSurname=true;
                }
            }
}
